
const FIRST_NAME = "FLORINA_DARIA";
const LAST_NAME = "GAGEA";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function initCaching() {
   var cache = {};
   cache.pageAccessCounter = function(page = 'home'){
       page=page.toLocaleLowerCase();
        if(page in cache){
            cache[page]=cache[page]+1;
        }
        else{
            cache[page]=1;
        }
    }
   cache.getCache= function(){
       return cache;
   }
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}