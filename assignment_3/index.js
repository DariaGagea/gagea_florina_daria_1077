
const FIRST_NAME = "FLORINA_DARIA";
const LAST_NAME = "GAGEA";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name,surname,salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

    getDetails() {
        return `${this.name} ${this.surname} ${this.salary}`;
    }
}

class SoftwareEngineer extends Employee {
   constructor(name,surname,salary,experience='JUNIOR'){
       super(name,surname,salary);
       this.experience=experience;
   }

   applyBonus(){
       if(this.experience === 'JUNIOR'){
            return this.salary + 0.1 * this.salary;
       }
       else if(this.experience === 'MIDDLE'){
            return this.salary + 0.15 * this.salary;
       }
       else if(this.experience === 'SENIOR'){
            return this.salary + 0.2 * this.salary;
       }
       else{
            return this.salary + 0.1 * this.salary;
       }
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}