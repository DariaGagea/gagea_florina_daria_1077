
const FIRST_NAME = "Florina_Daria";
const LAST_NAME = "Gagea";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(typeof value == "number"){
        if(value==+Infinity || value==-Infinity){
            return NaN;
        }
        if(value>=Number.MAX_VALUE || value<Number.MIN_VALUE){
            return NaN;
        }
        return parseInt(value);
    }
    if(typeof value == "string"){
        return parseInt(value);
    }
    if(value==NaN){
        return NaN;
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}


